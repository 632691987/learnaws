## 知识点

* 建立互联网网关, 并绑定到VPC上，使VPC子网可以连上互联网

## 官网

https://docs.aws.amazon.com/zh_cn/vpc/latest/userguide/VPC_Internet_Gateway.html

## 实战演习

+ 建立互联网网关: *vincent-study-igw*
+ 绑定到VPC *vincent-study-vpc* 上

