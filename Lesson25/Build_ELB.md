步骤：
* 建立目标组
* 建立ALB负载均衡器


### 建立目标组

+ Name: vincent-study-web-target-group

### 建立ALB负载均衡器

+ Name: vincent-study-web-alb


PS: 重点：创建出 ALB 以后， [DNS name] 就是共同的那个 IP 域名地址，下面是一个 example:
vincent-study-web-alb-1422862164.eu-west-3.elb.amazonaws.com