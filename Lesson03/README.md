申请AWS账号
==========

## 知识点

### 申请AWS账号的准备

* 邮件地址
* 手机号码
* 信用卡号

## 官网

https://aws.amazon.com/cn/

## 实战演习

1. 申请账号(SingUp, SingIn)
2. 设置CloudWatch
3. 设置IAM管理员(No Root)
4. 设置CloudTrail