## 知识点

* 设计高可用性网络结构

## 官网

https://aws.amazon.com/cn/elasticloadbalancing

## 实战演习

### Web端高可用性设计

> 看图说话

### 操作步骤

+ 建立新的可用区子网 - deeplearnaws-web-1c
+ 在新的子网区添加 web-ec2 实例
+ 通过 ELB 进行多区多实例之间的负载均衡