## 知识点

* 建立一个公网可访问的EC2服务器实例

## 实战演习

### EC2实例具体设置

+ Amazon Linux 2 AMI
+ t2.micro
+ vincent-study-vpc
+ vincent-study-web-1a
+ 自动分配公有 IP
+ 内网IP: 172.16.10.10/32
+ Name: vincent-study-web1
+ 密钥对: koma

### SSH连接

```bash
$ cp ~/Downloads/koma.pem ./
$ chmod 400 koma.pem
# Amazon Linux 2
$ ssh -i ./koma.pem ec2-user@[public address IP]
>
```