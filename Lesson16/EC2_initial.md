建立AMI - 把自己的系统克隆
=======================

## 知识点

* 在Amazon Linux 2上安装必要的软件包
* 打包Amazon Linux 2成AMI(Amazon Machine Image)
* 从AMI启动我们的EC2实例

## 官网

https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AMIs.html

############################## install docker ####################################
sudo yum update -y
sudo amazon-linux-extras install -y docker
sudo usermod -a -G docker ec2-user
sudo systemctl start docker
sudo systemctl status docker
sudo systemctl enable docker



############################## install docker-composer ####################################
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose



############################## install nodejs ####################################
curl -sL https://rpm.nodesource.com/setup_12.x | sudo bash -
sudo yum install -y gcc-c++ make
sudo yum install -y nodejs



############################## install git ####################################
sudo yum install git -y



############################## install nmap ####################################
sudo yum install -y nmap


### 将系统打包成AMI

+ Name: deeplearnaws-ami