这个是map route53 到 s3的

+ 设计域名
  - Name: blog.deeplearnaws.ml
+ 建立同名的 S3 存储桶
+ 上传网页文件到存储桶当中
+ 设置存储桶为静态网站公开
+ 设置 Route53, 关联 S3 存储桶
+ 确认动作
