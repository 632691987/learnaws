Route53 - 为我指引方向

* 使用 Route53 的DNS解析服务

## 官网

https://aws.amazon.com/route53/

## Route53特点

+ 高度可用且可靠
+ 灵活
+ 专为与其他 Amazon Web Services 配合使用而设计(A记录-ALIAS)
+ 简单
+ 速度快的
+ 经济高效
+ 安全
+ 可扩展
+ 简化混合云

## 实战演习

+ 申请域名
+ 设置名字服务器NS
+ 使用Route53路由