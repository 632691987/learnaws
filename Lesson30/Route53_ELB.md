* 使用 Route53 - 解析我的ELB服务

## 采用策略 - 简单路由

+ 简单路由策略
  - 对于为您的域执行给定功能的单一资源（例如为 example.com 网站提供内容的 Web 服务器），可以使用该策略。

## 实战演习

### 设置DNS解析

+ 启动ec2-web1, ecs2-web2
+ 确认ELB和目标组状态
+ 设置域名ELB解析
+ 域名访问