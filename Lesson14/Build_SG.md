## 知识点

* 网络存取控制列表 - ACL
* 安全组 - Security Group

## 实战演习

1. 确认ACL和SG的基本概念 - vincent-study-vpc
2. 建立 vincent-study 的 WEB 安全组

+ Name: vincent-study-web-sg
+ Port: SSH(22), HTTP(80), HTTPS(443)
