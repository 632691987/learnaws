## 知识点

* 理解路由表
* 建立路由表

https://docs.aws.amazon.com/zh_cn/vpc/latest/userguide/VPC_Route_Tables.html

### 建立路由表

+ RTB: vincent-study-web-rtb
  * VCP: vincent-study-vpc
  * Rule: 加入 vincent-study-igw
+ 把新路由绑定到 vincent-study-web-1a 子网


也就是建立两个路由表，一个是默认，在创建VPC的时候自动创建的。一个是手动创建的，建立以后是一模一样的，然后就在Edit Routes
那里添加 destination=0.0.0.0/0 target=igw-0101230758a535008

然后用 vincent-study-db-3a  subnet 链接到 原本的   vincent-study-rtb
然后用 vincent-study-web-3a subnet 链接到 新创建的 vincent-study-web-rtb