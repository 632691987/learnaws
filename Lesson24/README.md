高可用性设计 - 建立新的子网区
=========================

## 知识点

* 建立新的可用区子网
* 在新的子网区添加 web-ec2 实例

## 实战演习

> 看图说话

## 操作内容

* 建立新的可用区子网
  + Name: vincent-study-web-1c
  + 配置路由表
* 在新的子网区添加 web-ec2 实例
  + Name: vincent-study-web2

### 修改 web2 服务内容

```bash
$ docker exec -it vincent-study-web sh
>root $ vi app.js
...
$ docker container restart vincent-study-web
```