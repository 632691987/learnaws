## 实战步骤

+ 创建一个可以访问S3的角色
  - KomaRoleS3FullAccess
+ 在私有网段启动一个EC2实例, 赋予S3访问的角色
+ 在私有网段EC2中访问S3
+ 在VPC中建立一个网关终端节点，绑定到私有网段的路由表
+ 稍候片刻(添加到路由表需要时间)
+ 再次从私有网段中的EC2访问S3，确认动作

$ aws s3 ls --region ap-northeast-1



如果在一台不能出去，也就是没有EC2 所在的 SUBNET 所在的 route table 没有 IG 的情况下，想去访问 S3, 必须要有两个条件：
1, 拥有访问 S3 的权限
2, 创建一个 VPC -> Endpoint, 并且该endpoint 链接 EC2 所在的 vpc 和 subnet, 并且 [Service Name] 选择于 S3 相关的 Gateway 就可以了