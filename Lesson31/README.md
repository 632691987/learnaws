全球部署 - 新加坡：我来了
======================

## 知识点

* 在全球其他区域部署我们的Web服务

## 实战演习

+ 将东京区的AMI拷贝到新加坡区
+ 从新加坡区启动EC2实例
+ 配置安全组SG，使SSH和HTTP可以访问
+ 配置子网路由表
+ 修改索引显示内容

```bash
$ docker exec -it deeplearnaws-web sh
>root $ vi app.js
$ docker container restart deeplearnaws-web
```