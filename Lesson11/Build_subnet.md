## 知识点

* 建立公有网段
* 建立私有网段

### 子网所属VPC

+ vincent-study-vpc

### 公开网络

主要用于对外公开的服务器，如：Web，API服务器

+ 网段决定
  + 172.16.10.0/24
  + eu-west-3a
+ 标签
  * Name:vincent-study-web-3a

### 私有网络

主要用于内部使用的服务器，如：DB, Redis等

+ 网段决定
  + 172.16.20.0/24
  + eu-west-3a
+ 标签
  * Name:vincent-study-db-3a
