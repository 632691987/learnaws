## 知识点

* 在公开网络中建立NAT网关
* 建立DB私有网络的路由表, 设置NAT网关
* 建立DB私有网络的安全组, 开放 3306 - MySQL端口

### NAT网关

+ Name: vincent-study-web-nat
+ SubNet: vincent-study-web-1a
+ Elastic IP

### 路由表

+ Name: vincent-study-db-rtb
+ Target: vincent-study-web-nat -> 0.0.0.0/0

### 安全组

+ Name: vincent-study-db-sg
+ Port: 3306