## 路径

01. 组建规划网络
  + VPC
  + SubNet
  + ACL
  + SecurityGroup
  + RouteTable
02. 设计网络应用
  + EC2
  + ELB
  + Database
  + Auto Scaling
  + MultiAZ
  + MultiRegion
03. AWS服务
  + VPC
  + EC2
  + S3
  + IAM
  + RDS
  + DynamoDB
  + Lambda
  + API Gateway
  + Route 53
  + CloudTrail
  + CloudWatch
  + Elastic Beanstalk
  + SQS
  + ECR
  + ECS
04. 开发流程
  + CodeCommit
  + CodeBuild
  + CodeDeploy
  + CodePipeline
05. 进阶应用
  + Config
  + System Manager
  + CloudFormation
  + OpsWorks
  + IoT
  + 机器学习
  + 工作流
  + 移动开发(Amplify)
  + CLI
  + SDK(Node.js, Python, Go 等三种语言)

## 深学AWS

deeplearnaws.com

## 课程目标

帮助您完成一下系统架构能力。

+ 实现弹性架构
+ 实现高性能架构
+ 实现高可用架构
+ 实现安全的架构
+ 实现成本优化

## 会员区

+ 免费会员：技术讲解（AWS综合, 各种技术知识视频）
+ 一级会员：具体操作（VPC, RT, SG, ACL, EC2, ELB, ...）
+ 二级会员：专题讲解（Lambda, Amplify, DynamoDB, SQS, CI/CD, ...）
+ 三级会员：技术支持（有问题，找小马）
