## 知识点

* 修改私有网段安全组，增加 Port:22 支持
* 在私有网段建立 EC2 实例
* 通过公有网段 web-ec2 实例连接到 db-ec2 实例

### * 修改私有网段安全组，增加 Port:22 支持

+ vincent-study-db-sg
+ Port: 22

### 在私有网段建立 EC2 实例

+ Name: vincent-study-db1

### 通过公有网段 web-ec2 实例连接到 db-ec2 实例

+ 本地连接到 vincent-study-web1
+ 拷贝本地连接私钥到 vincent-study-web1
+ 通过 vincent-study-web1 连接到 vincent-study-db1
+ vincent-study-db1测试

```bash
# @vincent-study-web1
$ ssh -i ./vincent-study-ssh-key.pem 172.16.20.10
# @vincent-study-db1
$ sudo yum update -y
$ curl https://komavideo.com
```